#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> // access
#include <math.h>
#include <assert.h>
#include <cse6230rand.h>
#include <tictoc.h>
#include <omp.h>
#include "bd.h"

// Global variable for PRNG
cse6230nrand_t *nrand;

// return number of particles in trajectory file
int traj_read_npos(const char *filename)
{
    int npos;
    FILE *fp = fopen(filename, "r");
    fscanf(fp, "%d\n", &npos);
    assert(npos > 0);
    fclose(fp);
    return npos;
}

// read first frame of trajectory file
int traj_read(const char *filename, char *label, int npos, double *pos, int *types)
{
  int npos_read;
  FILE *fp = fopen(filename, "r");
  assert(fp);

  fscanf(fp, "%d\n", &npos_read);
  fgets(label, LINE_LEN, fp);

  assert(npos == npos_read);

  for (int i=0; i<npos; i++, pos+=3)
    fscanf(fp, "%d %lf %lf %lf\n", &types[i], &pos[0], &pos[1], &pos[2]);

  fclose(fp);

  return 0;
}

// append positions to trajectory file
int traj_write(const char *filename, const char *label, int npos, const double *pos, const int *types)
{
  FILE *fp = fopen(filename, "a");
  assert(fp);

  fprintf(fp, "%d\n", npos);
  fprintf(fp, "%s\n", label);

  for (int i=0; i<npos; i++, pos+=3)
    fprintf(fp, "%d %f %f %f\n", types[i], pos[0], pos[1], pos[2]);

  fclose(fp);

  return 0;
}

int main(int argc, char **argv)
{
  if (argc != 4)
  {
    fprintf(stderr, "usage: bd in.xyz out.xyz num_intervals\n");
    return -1;
  }

  char *input_filename  = argv[1];
  char *output_filename = argv[2];
  int num_intervals = atoi(argv[3]);

  if (access(output_filename, F_OK) != -1)
  {
    printf("Output file already exists: %s\nExiting...\n", output_filename);
    return -1;
  }

  int npos = traj_read_npos(input_filename);
  printf("Number of particles: %d\n", npos);
  printf("Number of intervals to simulate: %d\n", num_intervals);

  double *pos   = (double *) malloc(3*npos*sizeof(double));
  int    *types = (int *)    malloc(  npos*sizeof(int));
  assert(pos);
  assert(types);

  /* Generate random numbers for each thread */
  int max_threads = omp_get_max_threads();
  nrand = (cse6230nrand_t *)malloc(sizeof(cse6230nrand_t)*max_threads);
  for(int i = 0; i < max_threads; i++) {
    cse6230nrand_seed(i,&nrand[i]);
  }

  char label[LINE_LEN];
  double start_time, box_width;

  traj_read(input_filename, label, npos, pos, types);
  sscanf(label, "%lf %lf", &start_time, &box_width);
  printf("Simulation box width: %f\n", box_width);

  int maxnumpairs_p;
  {
    /* we are using $a = 1$ in this code */
    double pr_vol = (4./3.) * M_PI;
    double domain_vol = box_width*box_width*box_width;
    double vol_frac = pr_vol * npos / domain_vol;
    double interact_vol = 8 * pr_vol;
    double exp_ints_per = npos * interact_vol / domain_vol;
    printf("With %d particles of radius 1 and a box width of %f, the volume fraction is %g.\n",npos,box_width,vol_frac);
    printf("The interaction volume is %g, so we expect %g interactions per particle, %g overall.\n",interact_vol,exp_ints_per,exp_ints_per * npos / 2.);
    maxnumpairs_p = npos * ceil(exp_ints_per) * 10.;
    printf("Allocating space for %d interactions\n",maxnumpairs_p);
  }

  // Get number of max threads and store it
  int *max_threads_count = (int *)malloc(sizeof(int)*1);
  assert(max_threads_count);
  max_threads_count[0] = omp_get_max_threads();

  /* Initializing arrays to track distance as well as number of pairs between boxes and 
     within boxes. This should help in parallalizing the given algorithm */
  double **dist2_in_cube_p = (double **)malloc(sizeof(double *)*max_threads_count[0]);
  double **dist2_btw_cube_p = (double **)malloc(sizeof(double *)*max_threads_count[0]);
  int **pairs_inside_p = (int **)malloc(sizeof(int *)*max_threads_count[0]);
  int **pcount2_p = (int **)malloc(sizeof(int *)*max_threads_count[0]);
  int *pcount_p = (int *)malloc(sizeof(int)*max_threads_count[0]);
  int *pairs_cube_p = (int *)malloc(sizeof(int)*max_threads_count[0]);

  assert(dist2_in_cube_p);
  assert(dist2_btw_cube_p);
  assert(pairs_inside_p);
  assert(pcount2_p);
  assert(pcount_p);
  assert(pairs_cube_p);

  // init outside to stop reallocation or temporary allocation inside the loop
  int f;
  for(f = 0; f < max_threads_count[0]; f++) {
    dist2_in_cube_p[f] = (double *)malloc(sizeof(double)*maxnumpairs_p);
    dist2_btw_cube_p[f] = (double *)malloc(sizeof(double)*maxnumpairs_p);
    pairs_inside_p[f] = (int *)malloc(sizeof(int)*maxnumpairs_p*2);
    pcount2_p[f] = (int *)malloc(sizeof(int)*maxnumpairs_p*2);

    assert(dist2_in_cube_p[f]);
    assert(dist2_btw_cube_p[f]);
    assert(pairs_inside_p[f]);
    assert(pcount2_p[f]);
  }

  double t;
  TicTocTimer timer = tic();

  // simulate for num_intervals, writing frame after each interval
  for (int interval=1; interval<=num_intervals; interval++)
  {
    int retval;

    retval = bd(npos, pos, box_width, types, &maxnumpairs_p, &dist2_in_cube_p, &dist2_btw_cube_p, &pairs_inside_p, &pcount2_p, &pcount_p, &pairs_cube_p);
    if (retval) return retval;
    sprintf(label, "%f %f",
            start_time+interval*INTERVAL_LEN*DELTAT, box_width);
    traj_write(output_filename, label, npos, pos, types);

    if (interval % 100 == 0)
      printf("Done interval: %d\n", interval);
  }
  t = toc(&timer);
  printf("Time: %f for %d intervals\n", t, num_intervals);
  printf("Time per time step: %g\n", t/num_intervals/INTERVAL_LEN);

  // Cleanup all arrays created
  for(f = 0; f < max_threads_count[0]; f++) {
    free(dist2_in_cube_p[f]);
    free(dist2_btw_cube_p[f]);
    free(pairs_inside_p[f]);
    free(pcount2_p[f]);
  }
  free(max_threads_count);
  free(dist2_in_cube_p);
  free(dist2_btw_cube_p);
  free(pairs_inside_p);
  free(pcount2_p);
  free(pcount_p);
  free(pairs_cube_p);
  free(types);
  free(pos);

  return 0;
}