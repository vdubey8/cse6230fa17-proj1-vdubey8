
#CC = cc -pg
CC = cc
CSE6230UTILSDIR = ../../utils
# NOTE: on deepthought with the gnu4.4 compiler, you will have to add -std=gnu99 for harness to compile 
# correctly
CFLAGS = -g -Wall -O3 -std=gnu99
#CFLAGS = -g -Wall -O3
CPPFLAGS = -I$(CSE6230UTILSDIR) -I$(CSE6230UTILSDIR)/tictoc
OMPFLAGS = -fopenmp
LIBS = -lm
RM = rm -f
INFILE = lac1_novl2.xyz
#INFILE = 10k_particles.xyz
OUTFILE = out.xyz
NINTERVALS = 101
HARNESS_ENV = OMP_NUM_THREADS=16 OMP_PROC_BIND=TRUE

all: runharness

%.o: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) $(OMPFLAGS) -c -o $@ $<

harness: harness.o bd.o interactions.o
	$(CC) $(OMPFLAGS) -o $@ $^ $(LIBS)

runharness: harness
	$(RM) $(OUTFILE) && $(HARNESS_ENV) ./harness $(INFILE) $(OUTFILE) $(NINTERVALS)

clean:
	$(RM) *.o harness

.PHONY: clean runharness

