#include <cse6230rand.h>

#define INTERVAL_LEN 1000
#define DELTAT       1e-4
#define LINE_LEN     100

extern cse6230nrand_t *nrand;

int bd(int npos, double *pos, double L, const int *types, int *maxnumpairs_p, double ***dist2_in_cube_p,
       double ***dist2_btw_cube_p, int ***pairs_inside_p, int ***pcount2_p, int **pcount_p, int **pairs_cube_p);

/* see description in interactions.c */
int interactions(int           npos,
                 const double *pos,
                 double        L,
                 int           boxdim,
                 double        cutoff2,
                 double       **dist2_in_cube,
                 double       **dist2_btw_cube,
                 int          **pairs_inside,
                 int          **pcount2,
                 int           *pcount,
                 int           *pairs_cube,
                 int            maxnumpairs);