#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <omp.h>
#include "bd.h"

struct box
{
    int head;
};

// it is possible to use smaller boxes and more complex neighbor patterns
#define NUM_BOX_NEIGHBORS 13
int box_neighbors[NUM_BOX_NEIGHBORS][3] =
{
    {-1,-1,-1},
    {-1,-1, 0},
    {-1,-1,+1},
    {-1, 0,-1},
    {-1, 0, 0},
    {-1, 0,+1},
    {-1,+1,-1},
    {-1,+1, 0},
    {-1,+1,+1},
    { 0,-1,-1},
    { 0,-1, 0},
    { 0,-1,+1},
    { 0, 0,-1}
};

static int interactions_check(int npos, const double *pos, double L, double cutoff2, int numpairs, const int *pairs)
{
  int intcount = 0;

  for (int i = 0; i < npos; i++) {
    for (int j = i + 1; j < npos; j++) {
      double dx = remainder(pos[3*i+0] - pos[3*j+0],L);
      double dy = remainder(pos[3*i+1] - pos[3*j+1],L);
      double dz = remainder(pos[3*i+2] - pos[3*j+2],L);
      if (dx*dx + dy*dy + dz*dz < cutoff2) {
        intcount++;
        int k;
        for (k = 0; k < numpairs; k++) {
          if ((pairs[2*k + 0] == i && pairs[2*k + 1] == j) ||
              (pairs[2*k + 0] == j && pairs[2*k + 1] == i)) {
            break;
          }
        }
        assert(k<numpairs);
      }
    }
  }
  return intcount;
}

// interactions function
//
// Construct a list of particle pairs within a cutoff distance
// using Verlet cell lists.  The L*L*L domain is divided into
// boxdim*boxdim*boxdim cells.  We require cutoff < L/boxdim
// and boxdim >= 4.  Periodic boundaries are used.
// Square of distance is also returned for each pair.
// Note that only one of (i,j) and (j,i) are returned (not both).
// The output is not sorted by index in any way.
//
// npos = number of particles
// pos  = positions stored as [pos1x pos1y pos1z pos2x ...]
// L	= length of one side of box
// boxdim = number of cells on one side of box
// cutoff2 = square of cutoff
// distances2[maxnumpairs] = OUTPUT square of distances for particles
//						   within cutoff
// pairs[maxnumpairs*2] = OUTPUT particle pairs stored as
//						[pair1i pair1j pair2i pair2j ...]
// maxnumpairs = max number of pairs that can be stored in user-provided arrays
// numpairs_p = pointer to actual number of pairs (OUTPUT)
//
// function returns 0 if successful, or nonzero if error occured

int interactions(int npos, const double *pos, double L, int boxdim,
                 double cutoff2, double **dist2_in_cube,
                 double **dist2_btw_cube, int **pairs_inside,
                 int **pcount2, int *pcount,
                 int *pairs_cube, int maxnumpairs)

{

    if (boxdim < 4 || cutoff2 > (L/boxdim)*(L/boxdim))
    {
        printf("interactions: bad input parameters\n");
        return 1;
    }

	// 3D Box
    struct box b[boxdim][boxdim][boxdim];
    struct box *bp;
    struct box *neigh_bp;

    // box indices
    int idx, idy, idz;
    int nidx, nidy, nidz;

    // allocate memory for particles in each box
    for (idx=0; idx<boxdim; idx++) {
      for (idy=0; idy<boxdim; idy++) {
        for (idz=0; idz<boxdim; idz++) {
          b[idx][idy][idz].head = -1;
        }
      }
    }

    // allocate implied linked list
    int *next = (int *) malloc(npos*sizeof(int));
    if (next == NULL)
    {
        printf("interactions: could not malloc array for %d particles\n", npos);
        return 1;
    }

    // traverse all particles and assign to boxes
    int i;
    for (i=0; i<npos; i++)
    {
        double pos_p[3];
        // initialize entry of implied linked list
        next[i] = -1;

        for (int j = 0; j < 3; j++) {
          pos_p[j] = remainder(pos[3*i+j]-L/2.,L) + L/2.;
        }
        // which box does the particle belong to?
        idx = (int)(pos_p[0]/L*boxdim);
        idy = (int)(pos_p[1]/L*boxdim);
        idz = (int)(pos_p[2]/L*boxdim);

        // add to beginning of implied linked list
        bp = &b[idx][idy][idz];
        next[i] = bp->head;
        bp->head = i;
    }

    int p1, p2;
    double d2, dx, dy, dz;

	/* Allocating 0 to pair counts - inside and between boxes */
    int maxThreads = omp_get_max_threads();
    memset(pcount, 0, sizeof(int)*maxThreads);
    memset(pairs_cube, 0, sizeof(int)*maxThreads);

    /* Parallalizing interactions between pairs inside boxes and between box pairs
	
	Using the OpenMP collapse-clause to increase the total number of iterations that will be partitioned across 
	the available number of OMP threads by reducing the granularity of work to be done by each thread. 
	If the amount of work to be done by each thread is non-trivial (after collapsing is applied), 
	this may improve the parallel scalability of the OMP application. */
	
	//#pragma omp parallel for num_threads(16)
    #pragma omp parallel for collapse(3) private(idx,idy,idz,nidx,nidy,nidz,bp,p1,p2,d2,dx,dy,dz,neigh_bp)
    for (idx=0; idx<boxdim; idx++)
    {
        for (idy=0; idy<boxdim; idy++)
        {
            for (idz=0; idz<boxdim; idz++)
            {
                bp = &b[idx][idy][idz];

                // interactions within box
                p1 = bp->head;
                if(p1 == -1)
                  continue;

                int thread_id = omp_get_thread_num();

                while (p1 != -1)
                {
                    p2 = next[p1];
                    while (p2 != -1)
                    {

                        // do not need minimum image since we are in same box
                        dx = remainder(pos[3*p1] - pos[3*p2],L);
                        dy = remainder(pos[3*p1+1] - pos[3*p2+1],L);
                        dz = remainder(pos[3*p1+2] - pos[3*p2+2],L);

                        if ((d2 = dx*dx+dy*dy+dz*dz) < cutoff2)
                        {
                            pairs_inside[thread_id][2*pcount[thread_id]] = p1;
                            pairs_inside[thread_id][2*pcount[thread_id]+1] = p2;
                            dist2_in_cube[thread_id][pcount[thread_id]] = d2;
                            pcount[thread_id]++;
                        }

                        p2 = next[p2];
                    }
                    p1 = next[p1];
                }

                // interactions with other boxes
                int j;
                for (j=0; j<NUM_BOX_NEIGHBORS; j++)
                {
                    nidx = (idx + box_neighbors[j][0] + boxdim) % boxdim;
                    nidy = (idy + box_neighbors[j][1] + boxdim) % boxdim;
                    nidz = (idz + box_neighbors[j][2] + boxdim) % boxdim;

                    neigh_bp = &b[nidx][nidy][nidz];

                    p1 = neigh_bp->head;
                    while (p1 != -1)
                    {
                        p2 = bp->head;
                        while (p2 != -1)
                        {

                            // compute distance vector
                            dx = remainder(pos[3*p1+0] - pos[3*p2+0],L);
                            dy = remainder(pos[3*p1+1] - pos[3*p2+1],L);
                            dz = remainder(pos[3*p1+2] - pos[3*p2+2],L);

                            if ((d2 = dx*dx+dy*dy+dz*dz) < cutoff2)
                            {
                                pcount2[thread_id][2*pairs_cube[thread_id]] = p1;
                                pcount2[thread_id][2*pairs_cube[thread_id]+1] = p2;
                                dist2_btw_cube[thread_id][pairs_cube[thread_id]] = d2;
                                pairs_cube[thread_id]++;
                            }

                            p2 = next[p2];
                        }
                        p1 = next[p1];
                    }
                }
            }
        }
    }

    free(next);

#if 0
    {
      int numpairscheck = interactions_check(npos,pos,L,cutoff2,numpairs,pairs);

      assert(numpairs == numpairscheck);
    }
#endif

    return 0;
}

#if defined(INTERACTIONS_MEX)
#include "mex.h"

// matlab: function pairs = interactions(pos, L, boxdim, cutoff)
//  assumes pos is in [0,L]^3
//  assumes boxdim >= 4
//  pairs is an int array, may need to convert to double for matlab use
void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    int npos;
    const double *pos;
    double L;
    int boxdim;
    double cutoff;

    npos   = mxGetN(prhs[0]);
    pos    = mxGetPr(prhs[0]);
    L      = mxGetScalar(prhs[1]);
    boxdim = (int) mxGetScalar(prhs[2]);
    cutoff = mxGetScalar(prhs[3]);

    // estimate max number of pairs, assuming somewhat uniform distribution
    double ave_per_box = npos / (double)(boxdim*boxdim*boxdim) + 1.;
    int maxnumpairs = (int) (0.7*npos*ave_per_box*(NUM_BOX_NEIGHBORS+1) + 1.);
    printf("interactions: maxnumpairs: %d\n", maxnumpairs);

    // allocate storage for output
    int *pairs = (int *) malloc(2*maxnumpairs*sizeof(int));
    double *distances2 = (double *) malloc(maxnumpairs*sizeof(double));
    if (pairs == NULL || distances2 == NULL)
    {
        printf("interactions: could not allocate storage\n");
        return;
    }

    int numpairs; // actual number of pairs
    int ret;
    ret = interactions(npos, pos, L, boxdim, cutoff*cutoff, distances2,
                       pairs, maxnumpairs, &numpairs);
    if (ret != 0)
    {
        printf("interactions: error occured\n");
        if (ret == -1)
            printf("interactions: estimate of required storage was too low\n");
        return;
    }
    printf("interactions: numpairs: %d\n", numpairs);

    // allocate matlab output matrix
    plhs[0] = mxCreateDoubleMatrix(numpairs, 3, mxREAL);
    double *data = (double *) mxGetPr(plhs[0]);

    // first col of data array is row indices
    // second col of data array is col indices
    // third col of data array is distance2
    int i;
    for (i=0; i<numpairs; i++)
    {
        data[i]            = pairs[2*i];
        data[i+numpairs]   = pairs[2*i+1];
        data[i+numpairs*2] = distances2[i];
    }

    free(pairs);
    free(distances2);
}
#endif

#if defined(INTERACTIONS_MAIN)
int main()
{
    int npos = 3;
    double pos[] = {0., 0., 0.,  0., 0., 3.5,  0., 3.5, 0.};
    double L = 8;
    int pairs[1000*2];
    int numpairs;

    interactions(npos, pos, L, 8, 99., pairs, 1000, &numpairs);

    printf("numpairs %d\n", numpairs);

    return 0; }
#endif