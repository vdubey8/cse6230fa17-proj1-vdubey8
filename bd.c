#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> // access
#include <math.h>
#include <assert.h>
#include "bd.h"
#include <string.h>
#include <omp.h>


int bd(int npos, double *pos, double L, const int *types, int *maxnumpairs_p, double ***dist2_in_cube_p, double ***dist2_btw_cube_p, 
       int ***pairs_inside_p, int ***pcount2_p, int **pcount_p, int **pairs_cube_p)
{
  double f = sqrt(2.*DELTAT);
  /* 2 is twice the radius, $2 r_c$ in Prof. Chow's Lecture 5 */
  int     boxdim = L / 2;
  /* Must be at least the square of twice the radius */
  /* Constants being used for force calculation */
  double  cutoff2 = 4.;
  const double Mdt = DELTAT*1.0;
  const double a = 1.0;
  const double krepul = 100.;
  int maxnumpairs = *maxnumpairs_p;
  int max_threads = omp_get_max_threads();

  /* distance between pairs inside a cube */
  double **dist2_in_cube = *dist2_in_cube_p;
  int **pairs_inside = *pairs_inside_p;
  /* Number of pairs in a cube */
  int *pcount = *pcount_p;
  /* distance between cubes */
  double **dist2_btw_cube = *dist2_btw_cube_p;
  /* Number of pairs between cubes */
  int **pcount2 = *pcount2_p;
  int *pairs_cube = *pairs_cube_p;

  for (int step=0; step<INTERVAL_LEN; step++)
  {
    int retval;

    while (1) {
      retval = interactions(npos, pos, L, boxdim, cutoff2, dist2_in_cube,
                            dist2_btw_cube, pairs_inside, pcount2,
                            pcount, pairs_cube, maxnumpairs);
      if (!retval) break;
      if (retval == -1) {
        assert(0);
      } else {
        return retval;
      }
    }

    // Allocating zero value for initial forces
    double particle_forces[3*npos];
	#pragma omp parallel for schedule(static)
	for (int i=0; i<3*npos; i++)
		particle_forces[i] = 0.;


    // Force calculation in parallel
	// Using OMP ATOMIC/CRITICAL slows down the execution time of the code below in context of the given problem.
    #pragma omp parallel
    {
      #pragma omp for
      for(int t = 0; t < max_threads; t++) 
	  {
		/* Put the force calculation here using the same formula as exercise 04*/
		/* Step 1: interactions between pairs inside cubes */
		
		// Declaring these variable outside the loop gives significant speedup
        int thread_p = pcount[t];
        int _p;
		for(_p = 0; _p < thread_p; _p++) 
		{
			int p_j = pairs_inside[t][2*_p];
			int p_k = pairs_inside[t][2*_p+1];
			double dx = remainder(pos[3*p_j] - pos[3*p_k],L);
			double dy = remainder(pos[3*p_j+1] - pos[3*p_k+1],L);
			double dz = remainder(pos[3*p_j+2] - pos[3*p_k+2],L);
			
			double s = sqrt(dist2_in_cube[t][_p]);
			
			if (p_j == p_k){
				printf("no displacement");
			}
			// calculating repulsive forces
			double force = krepul * (2. * a - s);
			//#pragma omp atomic
			particle_forces[3*p_j] += force*dx/s;
			//#pragma omp atomic
			particle_forces[3*p_j+1] += force*dy/s;
			//#pragma omp atomic  
			particle_forces[3*p_j+2] += force*dz/s;
			//#pragma omp atomic
			particle_forces[3*p_k] -= force*dx/s;
			//#pragma omp atomic  
			particle_forces[3*p_k+1] -= force*dy/s;
			//#pragma omp atomic  
			particle_forces[3*p_k+2] -= force*dz/s;

        }
      }

        /* Step 2: interactions between two adjacent boxes */
        #pragma omp single
        {
          for(int t = 0; t < max_threads; t++) {
			// Declaring these variable outside the loop gives significant speedup
			int thread_p = pairs_cube[t];
            int _p;
            for(_p = 0; _p < thread_p; _p++) {
                int p_j = pcount2[t][2*_p];
                int p_k = pcount2[t][2*_p+1];
                double dx = remainder(pos[3*p_j] - pos[3*p_k],L);
                double dy = remainder(pos[3*p_j+1] - pos[3*p_k+1],L);
                double dz = remainder(pos[3*p_j+2] - pos[3*p_k+2],L);
				
                double s = sqrt(dist2_btw_cube[t][_p]);
			  
			    if (p_j == p_k){
			  	  printf("no displacement");
			    }
				// calculating repulsive forces
				double force = krepul * (2. * a - s);
				//#pragma omp atomic
                particle_forces[3*p_j] += force*dx/s;
				//#pragma omp atomic
                particle_forces[3*p_j+1] += force*dy/s;
				//#pragma omp atomic
                particle_forces[3*p_j+2] += force*dz/s;
				//#pragma omp atomic
                particle_forces[3*p_k] -= force*dx/s;
				//#pragma omp atomic
                particle_forces[3*p_k+1] -= force*dy/s;
				//#pragma omp atomic
                particle_forces[3*p_k+2] -= force*dz/s;

            }
          }
        }

        // update positions with Brownian displacements   
		#pragma omp for
        for (int i=0; i<3*npos; i++)
        {
          int thread_id = omp_get_thread_num();
          pos[i] += f*cse6230nrand(&nrand[thread_id]) + particle_forces[i]*Mdt;
        }
		
    }
  }
  
  *maxnumpairs_p = maxnumpairs;

  return 0;
}